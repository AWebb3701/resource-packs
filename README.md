# RPGInventory Resource Packs
<!-- NOTE: This file generated with script all changes will be replaced. DO NOT change this. -->

| RP version | MC version |
|------------|------------|
| v2         |  1.9-1.10  |
| v3         | 1.11-1.12  |
| v4         | 1.13-1.14  |
| v5         | 1.15+      |

### v2

| Name | Hash (SHA1) |
|------|-------------|
| [vanilla](out/v2-vanilla.zip) | 23a1f43705d94cef41a4851e6390afc4799f184e |

### v3

| Name | Hash (SHA1) |
|------|-------------|
| [vanilla-wooden](out/v3-vanilla-wooden.zip) | 45721c1f97f1f0be5d730db8392ab542728ad347 |
| [vanilla](out/v3-vanilla.zip) | 34b63f42ed78af98c6941dc45d32d8851fd59c6a |

### v4

| Name | Hash (SHA1) |
|------|-------------|
| [majesty](out/v4-majesty.zip) | 04f3a5ed3b3ebad6811d0fca435f217138c7de69 |
| [vanilla](out/v4-vanilla.zip) | 1f36449f98a090108c159844157dabcfe61aa99f |

### v5

| Name | Hash (SHA1) |
|------|-------------|
| [majesty](out/v5-majesty.zip) | ad71caf329a4e541347b6935b6cfaa9d4229c9d3 |
| [vanilla](out/v5-vanilla.zip) | 7f3bd3ea58e726033d50811e7ae9ba143f84d41c |
